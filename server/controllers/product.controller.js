const { connection } = require("../db.js"); 
const sql =  require("mssql");

export const getAllProducts = async (req, res) => {
    try {
      const pool = await connection();
      const result = await pool.request().query("SELECT * FROM producto");
      res.json(result.recordset);
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  };

 export const restarExistencia = async (req, res) => {
    //"A002",15
    const { cod_product, cantidad } = req.body;
    try {
      const pool = await connection();
      const result = await pool.request().query(`Execute SP_RestartExistencia ${cod_product},${cantidad}`);
      res.json(result.recordset);
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  };
  
export const registrarProducto = async (req, res) => {
    const { nombre, existencia, precio, url_image } = req.body;
    console.log(req.body);
    try {
      const pool = await connection();
      const result = await pool
        .request()
        .input("nombre", sql.VarChar(255), nombre)
        .input("existencia", sql.Int, existencia)
        .input("precio", sql.VarChar(10), precio)
        .input("url_image", sql.VarChar(255), url_image)
        .execute("SP_RegistrarProducto");
      res.json(result.recordset);
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  };

export const deleteProduct = async (req, res) => {
    const { id } = req.params;
    try {
      const pool = await connection();
      await pool
        .request()
        .input("idCod", sql.Int, id)
        .execute("SP_EliminarProducto");
      res.json({message:"delete"})
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  };

export const updateProduct =  async(req,res)=>{
    const { nombre, existencia,id_cod,cod_product } = req.body;
    try {
      const pool = await connection();
      const result = await pool
        .request()
        .input("idCod",sql.Int,id_cod)
        .input("codProd",sql.VarChar(255),cod_product) 
        .input("nombre", sql.VarChar(255), nombre) 
        .input("existencia", sql.Int, existencia)  
        .execute("SP_EditarProducto"); 
      res.json(result.recordset);
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  }

export const addExistencia = async (req, res) => {
    //"A002",15
    const { cod_product, cantidad } = req.body;
    try {
      const pool = await connection();
      const result = await pool.request()
        .query(`Execute SP_SumaExistencia ${cod_product},${cantidad}`);
      res.json(result.recordset);
    } catch (err) {
      console.error(err);
      res.status(500).send('Error en el servidor');
    }
  };