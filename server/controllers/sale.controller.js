const { connection } = require("../db.js");
const sql = require("mssql");

export const createSale = async (req, res) => {
  try {
    const { CI, DineroRecibido, Cambio, Total } = req.body;

    const pool = await connection();

    let nuevoID;

    const request = pool.request();
    request.input('CI', sql.VarChar(200), CI); // Cambia el tipo de dato a VARCHAR(200)
    request.input('DineroRecibido', sql.VarChar(200), DineroRecibido); // Cambia el tipo de dato a VARCHAR(200)
    request.input('Cambio', sql.VarChar(200), Cambio); // Cambia el tipo de dato a VARCHAR(200)
    request.input('Total', sql.VarChar(200), Total); // Cambia el tipo de dato a VARCHAR(200)
    request.output('NuevoID', sql.Int);

    const result = await request.execute('InsertarTransaccion');

    nuevoID = result.output.NuevoID;

    await pool.close();

    res.status(201).json({ message: 'Transacción creada exitosamente', newID: nuevoID });
  } catch (error) {
    console.error('Error al crear la transacción:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
};