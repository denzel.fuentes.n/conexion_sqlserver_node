const sql =  require("mssql");

const dbSettings = {
  user: "root",
  password: "root1234",
  server: "localhost",
  database: "inventario",
  options: {
    encrypt: true, 
    trustServerCertificate: true, 
  },
};
async function connection() {
    try {
      const pool = await sql.connect(dbSettings);
      return pool;
    } catch (error) {
      console.error(error);
    }
};


module.exports = {connection};
