const express = require("express");
const router  = express.Router();
import * as CtrlProduct from "../controllers/product.controller.js";

router.get("/data/all",CtrlProduct.getAllProducts);

router.post("/restar", CtrlProduct.restarExistencia);

router.post("/sumar",CtrlProduct.addExistencia);

router.post("/registrar/producto", CtrlProduct.registrarProducto);

router.delete("/eliminar/:id",CtrlProduct.deleteProduct);

router.post("/update",CtrlProduct.updateProduct);

module.exports = router;