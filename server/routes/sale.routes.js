const express = require("express");
const router  = express.Router();
import * as CtrlSale from "../controllers/sale.controller.js"

router.post('/post',CtrlSale.createSale);

export default router;