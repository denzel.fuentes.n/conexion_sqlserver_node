const express = require('express');
const app = express();
const port = process.env.PORT || 3000;



/* app.set('view engine','ejs');
app.set('views',__dirname + 'views');
*/
app.use(express.static('public'))
app.use(express.json())

const productRoutes = require("./routes/product.routes.js");
import saleRoutes from "./routes/sale.routes.js";

app.use(saleRoutes);
app.use(productRoutes);

app.listen(port, () => {
  console.log(`Servidor en ejecución en http://localhost:${port}`);
});