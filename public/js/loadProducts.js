
export default async function cargarDatosIniciales(mostrarProductos) {
    try {
        const response = await fetch("/data/all");
        if (response.ok) {
            const productos = await response.json();
            mostrarProductos(productos);
        } else {
            console.error("Error en la solicitud.");
        }
    } catch (error) {
        console.error(error);
    }
}