import loadProduct from "./loadProducts.js";
let productosList = {};
document.addEventListener("DOMContentLoaded", () => {
    $("#payment-form").submit(function (event) {
    event.preventDefault(); 
    registerSale();
    });
    
    function mostrarProductos(data) {
        console.log(data);
        data.forEach((producto)=> {
            var productoHTML = `
                <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                    <div class="card">
                        <img src="${producto.url_image}" class="card-img-top" alt="${producto.nombre}">
                        <div class="card-body">
                            <h5 class="card-title">${producto.nombre}</h5>
                            <p class="card-text">Precio: $${producto.precio}</p>
                            <button class="btn btn-primary" id="${producto.id_cod}">Agregar</button>
                        </div>
                    </div>
                </div>
            `;
            // Agrega el producto al contenedor
            $('#productos-container').append(productoHTML);
            $(`#${producto.id_cod}`).click(()=>{
                const cod_product = producto.cod_product;
                if (!productosList[`${cod_product}`]) {
                    productosList[`${cod_product}`] = {
                        nombre:producto.nombre,
                        cantidad:1,
                        total:parseInt(producto.precio),
                        precio:producto.precio
                    };
                    addProductToFactura(producto);
                }else{
                    productosList[`${cod_product}`].cantidad+=1
                    productosList[`${cod_product}`].total+=parseInt(producto.precio);
                    refreshProductByFactura(cod_product);
                }
                calculateTotal();               
            });
        });
    }

    function addProductToFactura(producto) {
      const table = document.querySelector(".table tbody");
      const newRow = document.createElement("tr");
      newRow.id = producto.cod_product;
      const productNameCell = document.createElement("td");
      productNameCell.textContent = producto.nombre;

      const quantityCell = document.createElement("td");
      quantityCell.textContent = 1;

      const unitPriceCell = document.createElement("td");
      unitPriceCell.textContent = "$"+producto.precio;

      const totalCell = document.createElement("td");
      totalCell.textContent ="$"+ producto.precio;

      const minusButton = document.createElement("button");
      minusButton.textContent = "-";
      minusButton.className = "btn btn-danger"; 
      minusButton.addEventListener("click", () => {
          const productOfList = productosList[producto.cod_product];
          if (productOfList.cantidad > 0) {
             
              quantityCell.textContent = productOfList.cantidad -= 1;
              console.log(productOfList);
              totalCell.textContent = "$"+(productOfList.total -= productOfList.precio); 
              calculateTotal(); 
              if(productOfList.cantidad == 0){
                $(`#${producto.cod_product}`).remove();
                delete productosList[producto.cod_product];
              }
          }
        });
      

      newRow.appendChild(productNameCell);
      newRow.appendChild(quantityCell);
      newRow.appendChild(unitPriceCell);
      newRow.appendChild(totalCell);
      newRow.appendChild(minusButton);
      table.appendChild(newRow);
    }

    function refreshProductByFactura(cod_product){
        const row  = $(`#${cod_product}`);
        console.log(row);
        const celdas = row.find('td');
        celdas.eq(1).text(productosList[cod_product].cantidad);
        celdas.eq(3).text("$"+productosList[cod_product].total);
    }

    function calculateTotal() {
      let total = 0;
      for (const key in productosList) {
        if (productosList.hasOwnProperty(key)) {
          const product = productosList[key];
          total += product.total;
        }
      }

      $("#total").text("$" + total);
    }

    function registerSale() {
      const CI = document.getElementById("ci").value;
      const DineroRecibido = document.getElementById("dineroRecibido").value;
      const Cambio = document.getElementById("cambio").value;
      const Total = document.getElementById("total").value;
      const data = {
          CI,
          DineroRecibido,
          Cambio,
          Total,
      };
  
      fetch("/post", {
          method: "POST",
          headers: {
              "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
      })
      .then((response) => {
          if (response.status === 201) {
              return response.json();
          } else {
              throw new Error("Error al crear la transacción");
          }
      })
      .then((data) => {
          console.log("Transacción creada exitosamente. Nuevo ID:", data.newID);
          alert("transaccion realizada")
      })
      .catch((error) => {
          console.error("Error:", error);
      });
    }
  
    loadProduct(mostrarProductos);
});