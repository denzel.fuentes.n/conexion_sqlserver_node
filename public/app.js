// app.js
import loadProduct from "./js/loadProducts.js";

document.addEventListener("DOMContentLoaded", () => {
    const operacionForm = document.getElementById("operacionForm");
    const operacionFormRegistrarProducto = document.getElementById("registroForm")
    const tablaProductos = document.getElementById("tablaProductos");

    operacionForm.addEventListener("submit", async (e) => {
        e.preventDefault();
        const cod_product = document.getElementById("cod_product").value;
        const operacion = document.getElementById("operacion").value;
        const cantidad = document.getElementById("cantidad").value;
        const ruta = operacion === "sumar" ? "/sumar" : "/restar";

        try {
            const response = await fetch(ruta, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ cod_product, cantidad }), 
            });

            if (response.ok) {
                const productos = await response.json();
                const producto =  productos[0];
                console.log(producto.id_cod);
                const id = producto.id_cod.toString(); 
                const row = document.getElementById(id);
                row.getElementsByTagName("td")[2].innerText = producto.existencia;
            } else {
                console.error("Error en la solicitud.");
            }
        } catch (error) {
            console.error(error);
        }
    });
    operacionFormRegistrarProducto.addEventListener("submit",async(e)=>{
        e.preventDefault();
        const nombre = document.getElementById("nombre").value;
        const existencia = document.getElementById("existencia").value;
        const url_image = document.getElementById("url_image").value;
        const precio = document.getElementById("precio").value;
        const ruta = "/registrar/producto";
        try {
            const response = await fetch(ruta, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ nombre, existencia,url_image,precio }), 
            });

            if (response.ok) {
                const productos = await response.json();
               mostrarProductos(productos)
            } else {
                console.error("Error en la solicitud.");
            }
        } catch (error) {
            console.error(error);
        }
    })

    function eliminarProducto(id) {
           const row = document.getElementById(id); // Obtén el elemento por su ID
           row.remove(); // Elimina el elemento del DOM
        // Realiza una solicitud DELETE al servidor
        fetch(`/eliminar/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        })
        .then((response) => {
            if (response.ok) {
                console.log("Producto eliminado con éxito");
            } else {
                console.error("Error al eliminar el producto");
            }
        })
        .catch((error) => {
            // Maneja errores de red u otros errores
            console.error("Error de red:", error);
        });
    }
    function mostrarProductos(productos) {
        productos.forEach((producto) => {
            const row = document.createElement("tr");
            console.log(producto)
            row.id = producto.id_cod;
            row.innerHTML = `
                <td>${producto.cod_product}</td>
                <td>${producto.nombre}</td>
                <td>${producto.existencia}</td>
                <td>${producto.precio}</td>
                <td>
                    <img src="${producto.url_image}" alt="" srcset="">
                </td>
                <td>
                    <button class="btn btn-danger eliminar-button" onclick="eliminarProducto(${producto.id_cod})">Eliminar</button>
                </td>
            `;
            const eliminarButton = row.querySelector('.eliminar-button');
            eliminarButton.addEventListener('click', () => {
                eliminarProducto(producto.id_cod);
            });
    
            row.addEventListener("click", () => {
                console.log("Producto seleccionado:", producto);
                $('#nombre').val(producto.nombre);
                $('#existencia').val( producto.existencia);
                $('#precio').val(producto.precio);
                $('#url_image').val(producto.url_image); 
            });
            tablaProductos.appendChild(row);
        }); 
    }
    loadProduct(mostrarProductos);
});
